<?php
// connecting styles and scripts
add_action( 'wp_enqueue_scripts', 'forgity_theme_scripts' );
function forgity_theme_scripts() {
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );
    wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/assets/css/fontawesome-all.css', ['bootstrap'] );
    wp_enqueue_style( 'header', get_template_directory_uri() . '/assets/css/header.css', ['font-awesome'] );
    wp_enqueue_style( 'footer', get_template_directory_uri() . '/assets/css/footer.css', ['header'] );
    wp_enqueue_style( 'style', get_stylesheet_uri(), ['footer'] );

    wp_enqueue_script( 'bootstrap-bundle-min', get_template_directory_uri() . '/assets/js/bootstrap.bundle.min.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'script', get_template_directory_uri() . '/assets/js/script.js', array('jquery'), '1.0.0', true );
}

// add images
add_theme_support('post-thumbnails');

// widgets for social icons
add_action( 'widgets_init', 'social_widgets_init' );
function social_widgets_init() {
    register_sidebar( array(
        'name'          => esc_html__( 'Social', 'rovetek' ),
        'id'            => 'social',
        'description'   => esc_html__( 'Add widgets here.', 'lemeor' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<span class="social">',
        'after_title'   => '</span>',
    ) );
}

require_once "includes/excerpt.php";