jQuery(document).ready(function($) {
//Languages
    $(".dropdown-menu li a").click(function () {
        $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
        $('.dropdown-menu li a i').removeClass('show-language');
        $(this).find('.fa-check').addClass('show-language');
    });

//Header-burger menu
    $('.nav-toggle').click(function () {
        $('.nav').addClass('move-menu');
        $('.nav-toggle').css({display: "none"});
    });

    $(document).mouseup(function (e)
    {
        var container = $(".header-adaptive > .nav");

        if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            container.removeClass('move-menu');
            $('.nav-toggle').css({display: "block"});
        }
    });

//Search adaptive
    $(".search-adaptive").click(function(){
        $(".wrap, .input").toggleClass("active");
        $("input[type='search']").focus();
    });
});

