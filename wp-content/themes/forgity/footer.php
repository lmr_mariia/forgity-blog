<footer class="site-footer">
    <div class="site-info">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-4 hidden-footer-item1">
                    <h4 class="h4-medium">Categories</h4>
                    <div class="line"></div>
                    <ul class="categories-footer">
                        <li><a href="#">Aut</a></li>
                        <li><a href="#">Maiores</a></li>
                        <li><a href="#">Sapiente</a></li>
                        <li><a href="#">Qui</a></li>
                        <li><a href="#">Quia</a></li>
                        <li><a href="#">Id</a></li>
                    </ul>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 hidden-footer-item1">
                    <h4 class="h4-medium">Tags</h4>
                    <div class="line"></div>
                    <div class="teg_cloud">
                        <div>
                            <a href="#"><i class="fas fa-tag"></i> Ratione</a>
                        </div>
                        <div>
                            <a href="#"><i class="fas fa-tag"></i> Impedit</a>
                        </div>
                        <div>
                            <a href="#"><i class="fas fa-tag"></i> Vero</a>
                        </div>
                        <div>
                            <a href="#"><i class="fas fa-tag"></i> Eos</a>
                        </div>
                        <div>
                            <a href="#"><i class="fas fa-tag"></i> Et</a>
                        </div>
                        <div>
                            <a href="#"><i class="fas fa-tag"></i> Sit</a>
                        </div>
                        <div>
                            <a href="#"><i class="fas fa-tag"></i> Optio</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 hidden-footer-item2">
                    <h4 class="h4-medium">Blog</h4>
                    <div class="line"></div>
                    <ul class="blog-footer">
                        <li><a><span>10.10.2019</span> Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</a></li>
                        <li><a><span>10.10.2019</span> Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</a></li>
                        <li><a><span>10.10.2019</span> Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</a></li>
                        <li><a><span>10.10.2019</span> Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</a></li>
                    </ul>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 col-12">
                    <h4 class="h4-medium">Information</h4>
                    <div class="line"></div>
                    <ul class="information-footer">
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Terms & Conditions</a></li>
                        <li><a href="#">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="line"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-7">
                                <div class="copyright">© 2019 FORGITY. Copyright.</div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-5">
                                <div class="social-icon">
                                    <a href="#"><i class="fab fa-facebook-square"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>