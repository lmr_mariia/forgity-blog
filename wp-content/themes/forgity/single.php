<?php get_header(); ?>
    <div class="wrap-content">
        <div class="container" style="min-height: 1000px;">
            <div class="row">
                <div class="col-lg-9 block-posts mt-5">
                    <div class="posts post-content mb-5">
                        <div class="post-img">
                            <?php the_post_thumbnail(); ?>
                        </div>
                        <h2 class="h2-thin mt-4"><?php the_title(); ?></h2>
                        <div class="container">
                            <div class="row mt-3">
                                <div class="col-6 post-category p-0">
                                    <p><?php the_category(' / '); ?></p>
                                </div>
                                <div class="col-6 post-date p-0">
                                    <p><?php the_time('M. j, Y'); ?></p>
                                </div>
                            </div>
                        </div>
                        <?php while ( have_posts()) : the_post(); ?>
                            <div class="posts-text"><?php the_content(); ?></div>
                        <?php endwhile; ?>
                        <div class="widget-icon mt-3 mb-5">
                            <?php if (is_active_sidebar('social')) : ?>
                                <?php dynamic_sidebar('social'); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 mt-5  block-sidebar">
                    <h5 class="mb-3">CATEGORIES</h5>
                    <ul class="category-post">
                        <?php
                        $cats = wp_list_categories('echo=0&&show_count=1&title_li=');
                        $cats = str_replace( ['(',')'], ['<span class="count-post">','</span>'], $cats );
                        echo $cats;
                        ?>
                    </ul>
                    <div class="line mb-4"></div>
                    <h5 class="mb-3">RECENT POSTS</h5>
                    <?php
                    $the_query = new WP_Query( array(
                        'posts_per_page' => 5,
                        'orderby' => array( 'date' => 'DESC' ),
                    ) );
                    while ( $the_query->have_posts() ) : $the_query->the_post();
                        ?>
                        <div class="resent-post mb-2">
                            <div class="post-img">
                                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                            </div>
                            <h2 class="h2-thin mt-2"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
                            <p class="post-date"><?php the_time('M. j, Y'); ?></p>
                            <div class="posts-text">
                                <?php echo kama_excerpt(array('maxchar' => 200)); ?>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>