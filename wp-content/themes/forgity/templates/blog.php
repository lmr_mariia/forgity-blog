<?php
/*
* Template name: blog
*
*/
?>

<?php get_header(); ?>
<div class="wrap-content">
    <section class="main-breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-6">
                    <h2 class="h2-thin"><?php the_title(); ?></h2>
                </div>
                <div class="col-6 text-right bread-crumbs">
                    <p><a href="#">Home</a>&nbsp / &nbspAll posts</p>
                </div>
            </div>
        </div>
    </section>

    <div class="container" style="min-height: 1000px;">
        <div class="row">
            <div class="col-lg-9 block-posts mt-5">
                <?php
                $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;

                $the_query = new WP_Query( array(
                    'posts_per_page' => 10,
                    'paged'          => $paged,
                ) );
                while ( $the_query->have_posts() ) : $the_query->the_post();
                    ?>
                    <div class="posts mb-5">
                        <div class="post-img">
                            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                        </div>
                        <div class="news_item_img_hidden text-center">
                            <a href="<?php the_permalink(); ?>" class="read-post">Read more</a>
                        </div>
                        <div class="container">
                            <div class="row mt-3">
                                <div class="col-6 post-category p-0">
                                    <p><?php the_category(' / '); ?></p>
                                </div>
                                <div class="col-6 post-date p-0">
                                    <p><?php the_time('M. j, Y'); ?></p>
                                </div>
                            </div>
                        </div>
                        <h2 class="h2-thin"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
                        <div class="posts-text">
                            <?php echo kama_excerpt(array('maxchar' => 210)); ?>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>

            <div class="col-lg-3 mt-5  block-sidebar">
                <h5 class="mb-3">CATEGORIES</h5>
                <ul class="category-post">
                    <?php
                    $cats = wp_list_categories('echo=0&show_count=1&title_li=');
                    $cats = str_replace( ['(',')'], ['<span class="count-post">','</span>'], $cats );
                    echo $cats;

                    ?>
                </ul>
                <div class="line mb-4"></div>
                <h5 class="mb-3">RECENT POSTS</h5>
                <?php
                $the_query = new WP_Query( array(
                    'posts_per_page' => 5,
                    'orderby' => array( 'date' => 'DESC' ),
                ) );
                while ( $the_query->have_posts() ) : $the_query->the_post();
                    ?>
                    <div class="resent-post mb-2">
                        <div class="post-img">
                            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                        </div>
                        <div class="news_item_img_hidden text-center">
                            <a href="<?php the_permalink(); ?>" class="read-post">Read more</a>
                        </div>

                        <h2 class="h2-thin mt-2"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>

                        <p class="post-date"><?php the_time('M. j, Y'); ?></p>

                        <div class="posts-text">
                            <?php echo kama_excerpt(array('maxchar' => 200)); ?>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
