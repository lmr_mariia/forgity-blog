<?php get_header(); ?>
<div class="wrap-content">
    <div class="container">
        <div class="row">
            <div class="col mt-5">
                <?php
                if ( have_posts()) :
                    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;

                    $the_query = new WP_Query( array(
                        'posts_per_page' => 10,
                        'paged'          => $paged,
                    ) );
                    while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <div class="posts mb-5">
                            <div class="post-img">
                                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                            </div>
                            <div class="news_item_img_hidden text-center">
                                <a href="<?php the_permalink(); ?>" class="read-post">Read more</a>
                            </div>
                            <div class="container">
                                <div class="row mt-3">
                                    <div class="col-6 post-category p-0">
                                        <p><?php the_category(' / '); ?></p>
                                    </div>
                                    <div class="col-6 post-date p-0">
                                        <p><?php the_time('M. j, Y'); ?></p>
                                    </div>
                                </div>
                            </div>
                            <h2 class="h2-thin"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
                            <div class="posts-text">
                                <?php echo kama_excerpt(array('maxchar' => 210)); ?>
                            </div>
                        </div>
                    <?php endwhile;
                else : ?>
                    <div class="container">
                        <div class="row text-center align-items-start justify-content-center">
                            <div class="col-lg-8 results-nothing">
                                <p>Sorry, but nothing matched your search criteria.<br>
                                    Please try again with some different keywords.</p>
                                <p><a href="javascript:history.back()">Go back</a>, or return to <a href="{{ url('/') }}"> forgity home page </a> to choose a new page.</p>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
