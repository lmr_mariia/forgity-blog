<?php get_header(); ?>
<div class="wrap-content">
    <div class="container error-page" style="min-height: 500px;">
        <div class="row">
            <div class="col-lg-12 text-center ">
                <h1>Oops!</h1>
                <p class="mb-5">WE CAN'T SEEM TO FIND THE PAGE YOU'RE LOOKING FOR.</p>

                <a href="#" class="button-style">To home page</a>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>