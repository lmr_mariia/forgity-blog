<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri() . '/assets/img/favicon.png'; ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css">
    <!-- Title -->
    <title><?php bloginfo('name'); ?><?php wp_title('|'); ?></title>
    <?php wp_head(); ?>
</head>
<body>

<header id="site-navigation" class="site-header bg-header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-5 col-5 mt-4 mb-4">
                <a class="logo" href="#"><img class="logo" src="<?php bloginfo("template_url"); ?>/assets/img/logo.svg"></a>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-7 col-7 text-right mt-3 mb-3">
                <div class="header-navigation">
                    <div class="navigation-search">
                        <form role="search" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="GET" id="search">
                            <?php get_search_form(); ?>
                            <button type="submit"><i class="fas fa-search"></i></button>
                        </form>
                    </div>
                    <div class="navigation-language">
                        <form action="/language" method="GET" id="language">
                            <div class="dropdown">
                                <i class="fas fa-globe-europe"></i>
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    English
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                    <li><a href="#" data-value="english"><i class="fas fa-check show-language"></i> English</a></li>
                                    <li><a href="#" data-value="espanol"><i class="fas fa-check"></i> Espanol</a></li>
                                    <li><a href="#" data-value="deutsch"><i class="fas fa-check"></i> Deutsch</a></li>
                                </ul>
                            </div>
                        </form>
                    </div>
                    <div class="cart">
                        <a href="#">
                            <i class="fas fa-shopping-cart"></i>
                        </a>
                    </div>
                    <div class="navigation-menu">
                        <nav class="main-navigation">
                            <ul class="menu">
                                <li><a href="#">Templates</a></li>
                                <li><a href="<?php echo get_page_link( 7 ); ?>">Blog</a></li>
<!--                                @guest-->
                                <li><a href="#">Log in</a></li>
<!--                                @else-->
<!--                                <li id="account" class="dropdown">-->
<!--                                    <button type="button" class="dropdown-toggle" data-toggle="dropdown">Account')}}</button>-->
<!--                                    <div class="dropdown-menu">-->
<!--                                        <div class="arrow"></div>-->
<!--                                        <span class="dropdown-item-text">@if(\Auth::user()->name != NULL)Hi, {{\Auth::user()->name}}@else Hi!@endif</span>-->
<!--                                        <a class="dropdown-item" href="#"><i class="fas fa-user"></i> Profile</a>-->
<!--                                        <a class="dropdown-item" href="#"><i class="fas fa-cog"></i> Settings</a>-->
<!--                                        <a class="dropdown-item" href="#"><i class="fas fa-folder-open"></i> My templates</a>-->
<!--                                        <a class="dropdown-item" href="#"><i class="fas fa-heart"></i> Favorites</a>-->
<!--                                        <div class="dropdown-divider"></div>-->
<!--                                        <a class="dropdown-item" href="#"><i class="fas fa-power-off"></i> Log out</a>-->
<!--                                    </div>-->
<!--                                </li>-->
<!--                                @endguest-->
                            </ul>
                        </nav>
                    </div>
                </div>

                <div class="header-adaptive">
                    <div class="wrap">
                        <form role="search" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="GET" id="search">
                            <?php get_search_form(); ?>
                        </form>
                        <i class="fa fa-search search-adaptive" aria-hidden="true"></i>
                    </div>
                    <div class="cart">
                        <a href="#">
                            <i class="fas fa-shopping-cart"></i>
                        </a>
                    </div>
                    <input type="checkbox" id="nav-toggle" hidden>

                    <nav class="nav">
                        <label class="nav-toggle"></label>
                        <div class="navigation-language">
                            <form action="/language" method="GET" id="language">
                                <div class="dropdown">
                                    <i class="fas fa-globe-europe"></i>
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        English
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                        <li><a href="#" data-value="english"><i class="fas fa-check show-language"></i> English</a></li>
                                        <li><a href="#" data-value="espanol"><i class="fas fa-check"></i> Espanol</a></li>
                                        <li><a href="#" data-value="deutsch"><i class="fas fa-check"></i> Deutsch</a></li>
                                    </ul>
                                </div>
                            </form>
                        </div>
                        <div class="line"></div>
                        <ul>
                            <li><a href="#">Templates</a></li>
                            <li><a href="#">Blog</a></li>
<!--                            @guest-->
                            <li><a href="#">Log in</a></li>
<!--                            @else-->
<!--                            <li><a href="#">Profile</a></li>-->
<!--                            <li><a href="#">Settings</a></li>-->
<!--                            <li><a href="#">My templates</a></li>-->
<!--                            <li><a href="#">Favorites</a></li>-->
<!--                            <li><a href="#">Log out</a></li>-->
<!--                            @endguest-->
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>