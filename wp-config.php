<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'wp_forgity' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'root' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', '' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '&;&+_KrW]Q)2dO})+{0v/LA(x>drobs**^*HtUlGluTQ(5{ccwp5,~s 6gnAt!Nr' );
define( 'SECURE_AUTH_KEY',  '))crjcLCK0R@-ma9]n}ID#6Oc4T$OSC[p!xO56[L#-`3f<Ay>M)gP_nfi9)V/Mrh' );
define( 'LOGGED_IN_KEY',    '38B.q<#fQ;rWgV?q(=I7/*8+c(=f39T8Hlb*^Jsi+OI7]=QGLzDoG9V${0=m|.f6' );
define( 'NONCE_KEY',        '1+iS*9sST1F*am<{h* f^:y/9)L2u,l~nSl*.h_KzSB.x#y^woMQkk(%gr?IX=i2' );
define( 'AUTH_SALT',        '4fQfHx+<}jiulJ@G^8JFftv8>~ +%$P${G%2orKY;7Bx:M OqTf6D[;:Mpl-mk8s' );
define( 'SECURE_AUTH_SALT', 'lmRS1X gi/.=<_17|66;M T94w=Da({<AoI3V4gp1{OwDC$=vpTyFpEbNM7(kLtC' );
define( 'LOGGED_IN_SALT',   '3x(`n_/tP`{gWNxuYiH(=4 B)fUc:`HMG%)Opt:;{fpf7N(_!Fl:%uk|{AgNz),6' );
define( 'NONCE_SALT',       '>P]x!b8:2?a[Q?L$m]U?.@LGppL/8VVXf$G_mMcLVY!Gu+}Lz]#ClsQ*|&aON`X:' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
